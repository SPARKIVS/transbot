#!/bin/python3 

import re, requests, time, sys, os, threading, random
from bs4 import BeautifulSoup

URL = 'https://www.bing.com/dict/search?q='
counter = 0
bing = {
    "Accept": "*/*",
    "Accept-Encoding": "gzip, deflate",
    "Host": "www.bing.com",
    "Connection": "keep-alive",
    "User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6",
    "Referer": "https://www.bing.com/"
}
cambridge = {
    "Accept": "*/*",
    "Accept-Encoding": "gzip, deflate",
    "Host": "dictionary.cambridge.org",
    "Connection": "keep-alive",
    "User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6",
    "Referer": "https://dictionary.cambridge.org"
}
dictionary = {
    "Accept": "*/*",
    "Accept-Encoding": "gzip, deflate",
    "Host": "www.dictionary.com",
    "Connection": "keep-alive",
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36",
    "Referer": "https://www.dictionary.com/"
}
dictionary1 = {
    "Accept": "*/*",
    "Accept-Encoding": "identity;q=1, *;q=0",
    "Host": "static.sfdict.com",
    "DNT": "1",
    "Range": "bytes=0-",
    "Sec-Fetch-Mode": "no-cors",
    "User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6",
    "Referer": "https://static.sfdict.com/audio/C00/C0095200.mp3"
}
proxies = {
    "http": "http://127.0.0.1:7890",
    "http": "http://127.0.0.1:1080"
}
monitor = []
hit = []

def read():
	try:
		option = sys.argv[1]
	except:
		return 0
	net = getChinese(option)
	print(net)
	#getPronunciation(option)

def readfile():
	global counter, monitor
	tasks = []
	try:
		filename = sys.argv[1]
	except:
		return 0
	try:
		option = sys.argv[2].lower()
	except:
		option = 'false'
	f = open(filename, 'r')
	try:
		lines = f.readlines()
	except Exception as err:
		f.close()
		print(err)
		return
	else:
		f.close()

	num_words = len(lines)

	sys.stdout.write(f'\r\033[1;32m[+]\033[0m {num_words} Tasks to process\n')
	sys.stdout.flush()
	if option == 'true':
		for line in lines:
			line = line.replace('\n', '')
			task = threading.Thread(target=getPronunciation, args=(line, option))
			tasks.append(task)
		for eachtask in tasks:
			eachtask.start()
		for eachtask in tasks:
			eachtask.join()
		print("\nHit: ", len(hit))
	else:
		for line in lines:
			line = line.replace('\n', '')
			eng, url = getPronunciation(line, option, 'dictionary')
			print("Word: %s URL: %s" % (eng, url))

	failed_num = 0
	indicator = False

	while option == 'true':
		failed = monitor
		if failed_num == len(monitor):
			if indicator:
				break
			else:
				indicator = True
		failed_num = len(monitor)
		monitor = []
		tasks = []
		time.sleep(random.randint(10, 20))
		sys.stdout.write(f'\r\033[1;32m[+]\033[0m Retrying {failed_num} failed task ...\n')
		sys.stdout.flush()
		counter = 0
		for word in failed:
			if indicator:
				task = threading.Thread(target=getPronunciation, args=(word, option, 'dictionary'))
			else:
				task = threading.Thread(target=getPronunciation, args=(word, option, 'dictionary'))
			tasks.append(task)
		for eachtask in tasks:
			eachtask.start()
		for eachtask in tasks:
			eachtask.join()
	write(monitor)

def getChinese(eng, stand_alone=True):
	pattern = "<ul>.*<\/span><\/li><\/ul>"
	url = URL + eng
	net = ''
	for i in range(5):
		try:
			response = requests.get(url, headers=bing, proxies=proxies, timeout=10)
		except:
			random.randint(10, 20)
			break

		if response.status_code == 200:
			rawContent = response.text
			raw_ul = re.findall(pattern, rawContent)
			try:
				soup = BeautifulSoup(raw_ul[0], features="lxml")
			except IndexError:
				print("  No result: ", eng)
				return ''
			li_list = soup.find_all('li')
			text = soup.text
			net = li_list.pop()

			try:
				net = net.text.split('网络')
			except:
				net = net.text
			else:
				if stand_alone:
					net = "\033[1;32m""网络 ""\033[0m" + net[-1]
				else:
					net = " *网络* " + net[-1]

			output = ''
			for li in li_list:
				wcls = li.find('span').text
				try:
					txt = li.text
					wdef = txt.split(wcls)[-1]
				except Exception as err:
					wdef = li.text
					print(err)
				if stand_alone:
					print("\033[1;32m" + wcls + "\033[0m" + ' ' + wdef)
				else:
					if len(wcls) > 0:
						output = output + '*' + wcls + '*' + ' ' + wdef + ' '
					else:
						output = output + wdef + ' '

			if not stand_alone:	
				#getPronunciation(eng)
				if len(output) < 2:
					output = net
				return output
			#getPronunciation(eng)
			break
		else:
			print('Request failed, Retry in 8s')
			time.sleep(random.randint(1, 8))

	return net

def downloader(url, filename, website):
	try:
		response = requests.get(url, headers=website, proxies=proxies, timeout=8)
	except Exception as err:
		#print(err)
		return False
	else:
		#print(response.status_code)
		if response.status_code != 404:
			with open(filename, 'wb') as file:
				file.write(response.content)
			return True
		else:
			return False

def getPronunciation(word, option, web="cambridge"):
	global proxies, counter, failed, monitor, hit
	base_url = "https://dictionary.cambridge.org"
	query = "/dictionary/english/" + word
	pattern = "\/media\/english\/us.*\.mp3"
	MP3_FILENAME_EXTENSION = '.mp3'
	DIR_PATH = 'src/'
	status = False
	index = 0

	headers = cambridge
	url = base_url + query
	fullname = word + MP3_FILENAME_EXTENSION
	filename = os.path.join(DIR_PATH, fullname)

	if web == "dictionary":
		base_url = ''
		url = "https://www.dictionary.com/browse/" + word
		headers = dictionary
		pattern = "<audio.*</audio>"
	try:
		response = requests.get(url, headers=headers, proxies=proxies, timeout=8)
	except:
		pass
	else:
		if response.status_code == 200:
			rawContent = response.text
			mp3links = re.findall(pattern, rawContent)
			num_mp3 = len(mp3links)

			if num_mp3 > 0:
				if web == "dictionary":
					headers = dictionary1
					soup = BeautifulSoup(mp3links[0], features="lxml")
					source = soup.find_all('source')[1]
					mp3_url = source.get('src')
				else:
					mp3_url = base_url+mp3links[0]
				for i in range(5):
					status = downloader(mp3_url, filename, headers)
					if status:
						hit.append(word)
						break
					else:
						time.sleep(random.randint(1, 10))
	finally:
		if not status:
			try:
				monitor.append(word)
			except ValueError as error:
				print(word)

	if option == 'false':
		try:
			bug_url = mp3_url
		except UnboundLocalError:
			bug_url = 'None'
		print("Hit: ", len(hit))
		return word, bug_url
	else:
		counter = counter + 1
		sys.stdout.write(f'\r\033[1;32m[+]\033[0m Downloading: {counter}')
		sys.stdout.flush()
	return 0

def write(wordset):
	with open('failed.txt', 'w+') as file:
		for i in wordset:
			file.write(i+'\n')

readfile()
#read()
