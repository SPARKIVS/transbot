#!/usr/bin/env python3

from subprocess import Popen, PIPE
from bs4 import BeautifulSoup
from time import sleep
import sys, re, os, requests, random, asyncio

bing = {
    "Accept": "*/*",
    "Accept-Encoding": "gzip, deflate",
    "Host": "www.bing.com",
    "Connection": "keep-alive",
    "User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6",
    "Referer": "https://www.bing.com/"
}
cambridge = {
    "Accept": "*/*",
    "Accept-Encoding": "gzip, deflate",
    "Host": "dictionary.cambridge.org",
    "Connection": "keep-alive",
    "User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6",
    "Referer": "https://dictionary.cambridge.org"
}
dictionary1 = {
    "Accept": "*/*",
    "Accept-Encoding": "gzip, deflate",
    "Host": "www.dictionary.com",
    "Connection": "keep-alive",
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36",
    "Referer": "https://www.dictionary.com/"
}
dictionary2 = {
    "Accept": "*/*",
    "Accept-Encoding": "identity;q=1, *;q=0",
    "Host": "static.sfdict.com",
    "DNT": "1",
    "Range": "bytes=0-",
    "Sec-Fetch-Mode": "no-cors",
    "User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6",
    "Referer": "https://static.sfdict.com/audio/C00/C0095200.mp3"
}

URL = 'https://www.bing.com/dict/search?q='
WORDLIST_FILE = "wordlist.txt"
wordlist = []
count = 0
state = 0

# filter word from raw input from clipboard
def filter(raw):
    p = u'[\u4e00-\u9fa5].'
    nonword1 = [',', '<', '.', '>', '/', '?', ';', ':', '\'', '"', '[', '{', ']', '}', '\\', '|', '=', '+', '_']
    nonword2 = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')']
    for nw in nonword1:
        if nw in raw:
            return False
    for nw in nonword2:
        if nw in raw:
            return False
    if re.search(p, raw):
        return False
    if len(raw) == 0:
        return False
    return True

# monitoring change of clipboard
async def watch():
    global wordlist, count, state
    # indicator = "\033[44m         \033[0m"
    args = ['xclip', '-selection', 'c', '-o']

    while True:
        try:
            p = Popen(args, stdout=PIPE, stderr=PIPE)
            out, err = p.communicate(1)
            out = out.decode()
            out = out.lower()

            if filter(out) and out not in wordlist:
                indicator = "\033[44m\033[1;30m PENDING \033[0m"
                wordlist.append(out)
                count += 1
                sys.stdout.write("\r[\033[1;32m+\033[0m] Eat {} new words |{}|".format(count, indicator))
                await getCambridgePronu(out)

            if state == 1:
                indicator = "\033[42m\033[1;97m SUCCESS \033[0m"
            elif state == 2:
                indicator = "\033[41m\033[1;97m FAILED  \033[0m"
            else:
                indicator = "\033[44m\033[1;30m PENDING \033[0m"
                # print("\033[43m\033[1;30m 404 \033[0m")

            sys.stdout.write("\r[\033[1;32m+\033[0m] Eat {} new words |{}|".format(count, indicator))
            sys.stdout.flush()
            if state == 0:
                sleep(5)
        except KeyboardInterrupt:
            save()
            break
    return

def write():
    pass

# take a given word and fetch translation & audio of it
def fetch(word):
    pass

# load wordlist.txt into wordlist list
def load():
    global wordlist, WORDLIST_FILE

    try:
        f = open(WORDLIST_FILE, 'r')
        eachlines = f.readlines()
        for word in eachlines:
            wordlist.append(word.strip("\n"))
    except FileNotFoundError:
        f = open(WORDLIST_FILE, 'w')
    finally:
        f.close()

# save wordlist list into file wordlist.txt
def save():
    global wordlist, count, WORDLIST_FILE

    try:
        f = open(WORDLIST_FILE, 'w')
        wordlist = set(wordlist)
        for word in wordlist:
            word = word.strip()
            if len(word) > 0:
                f.write(word)
                f.write('\n')
    except Exception as err:
        print(err)
    finally:
        f.close()

# get chinese translation from bing
def getChinese(eng, stand_alone=True):
    pattern = "<ul>.*<\/span><\/li><\/ul>"
    url = URL + eng
    net = ''
    for i in range(5):
        try:
            response = requests.get(url, headers=bing, proxies=proxies, timeout=10)
        except:
            random.randint(10, 20)
            break

        if response.status_code == 200:
            rawContent = response.text
            raw_ul = re.findall(pattern, rawContent)
            try:
                soup = BeautifulSoup(raw_ul[0], features="lxml")
            except IndexError:
                print("  No result: ", eng)
                return ''
            li_list = soup.find_all('li')
            text = soup.text
            net = li_list.pop()

            try:
                net = net.text.split('网络')
            except:
                net = net.text
            else:
                if stand_alone:
                    net = "\033[1;32m""网络 ""\033[0m" + net[-1]
                else:
                    net = " *网络* " + net[-1]

            output = ''
            for li in li_list:
                wcls = li.find('span').text
                try:
                    txt = li.text
                    wdef = txt.split(wcls)[-1]
                except Exception as err:
                    wdef = li.text
                    print(err)
                if stand_alone:
                    print("\033[1;32m" + wcls + "\033[0m" + ' ' + wdef)
                else:
                    if len(wcls) > 0:
                        output = output + '*' + wcls + '*' + ' ' + wdef + ' '
                    else:
                        output = output + wdef + ' '

            if not stand_alone: 
                #getPronunciation(eng)
                if len(output) < 2:
                    output = net
                return output
            #getPronunciation(eng)
            break
        else:
            print('Request failed, Retry in 8s')
            sleep(random.randint(1, 8))

    return net

# download mp3 audio file from given url
def downloader(url, filename, website):
    try:
        # response = requests.get(url, headers=website, proxies=proxies, timeout=8)
        response = requests.get(url, headers=website, timeout=8)
    except Exception as err:
        #print(err)
        return False
    else:
        #print(response.status_code)
        if response.status_code != 404:
            with open(filename, 'wb') as file:
                file.write(response.content)
            return True
        else:
            return False

# fecth a given word's pronunciation from Cambrige.com
async def getCambridgePronu(word):
    global state
    base_url = "https://dictionary.cambridge.org"
    query = "/dictionary/english/" + word
    pattern = "\/media\/english\/us.*\.mp3"
    status = False
    MP3_FILENAME_EXTENSION = '.mp3'
    DIR_PATH = 'audio/'

    headers = cambridge
    url = base_url + query
    fullname = word + MP3_FILENAME_EXTENSION
    filename = os.path.join(DIR_PATH, fullname)

    try:
        # fetch mp3 audio link
        # response = requests.get(url, headers=headers, proxies=proxies, timeout=8)
        response = requests.get(url, headers=headers, timeout=8)
    except:
        pass
    else:
        if response.status_code == 200:
            mp3links = re.findall(pattern, response.text)

            if len(mp3links) > 0:
                mp3_url = base_url + mp3links[0]

                # fetch audio file
                for i in range(2):
                    # call audio file downloader function here
                    status = downloader(mp3_url, filename, headers)
                    if status:
                        # hit.append(word)
                        state = 1
                        break
                    else:
                        sleep(random.randint(1, 10))
    finally:
        if not status:
            # print(mp3_url)
            # print(word)
            state = 2
    return

# fecth a given word's pronunciation from Dictionary.com
def getDictionaryPronu(word):
    global state
    MP3_FILENAME_EXTENSION = '.mp3'
    DIR_PATH = 'audio/'
    status = False

    fullname = word + MP3_FILENAME_EXTENSION
    filename = os.path.join(DIR_PATH, fullname)


    headers = dictionary1
    url = "https://www.dictionary.com/browse/" + word
    pattern = "<audio.*</audio>"

    try:
        # response = requests.get(url, headers=headers, proxies=proxies, timeout=8)
        response = requests.get(url, headers=headers, timeout=8)
    except:
        pass
    else:
        if response.status_code == 200:
            rawContent = response.text
            mp3links = re.findall(pattern, rawContent)
            num_mp3 = len(mp3links)

            if num_mp3 > 0:
                headers = dictionary2
                soup = BeautifulSoup(mp3links[0], features="lxml")
                source = soup.find_all('source')[1]
                mp3_url = source.get('src')

                print(mp3_url)
                # fetch audio file with 5 retries
                for i in range(2):
                    # call audio file downloader function here
                    status = downloader(mp3_url, filename, headers)
                    if status:
                        state = 1
                        # hit.append(word)
                        break
                    else:
                        sleep(random.randint(1, 10))
    finally:
        if not status:
            # print(mp3_url)
            # print(word)
            state = 2
    return

# Deprecated
# get prounciation of a given word from cambridge
def getPronunciation(word, option, web="cambridge"):
    global proxies, counter, failed, monitor, hit

    base_url = "https://dictionary.cambridge.org"
    query = "/dictionary/english/" + word
    pattern = "\/media\/english\/us.*\.mp3"
    MP3_FILENAME_EXTENSION = '.mp3'
    DIR_PATH = 'audio/'
    status = False
    index = 0

    headers = cambridge
    url = base_url + query
    fullname = word + MP3_FILENAME_EXTENSION
    filename = os.path.join(DIR_PATH, fullname)

    if web == "dictionary":
        base_url = ''
        url = "https://www.dictionary.com/browse/" + word
        headers = dictionary
        pattern = "<audio.*</audio>"
    try:
        # response = requests.get(url, headers=headers, proxies=proxies, timeout=8)
        response = requests.get(url, headers=headers, timeout=8)
    except:
        pass
    else:
        if response.status_code == 200:
            rawContent = response.text
            mp3links = re.findall(pattern, rawContent)
            num_mp3 = len(mp3links)

            if num_mp3 > 0:
                if web == "dictionary":
                    headers = dictionary1
                    soup = BeautifulSoup(mp3links[0], features="lxml")
                    source = soup.find_all('source')[1]
                    mp3_url = source.get('src')
                else:
                    mp3_url = base_url+mp3links[0]
                for i in range(5):
                    # call audio file downloader function here
                    status = downloader(mp3_url, filename, headers)
                    if status:
                        hit.append(word)
                        break
                    else:
                        sleep(random.randint(1, 10))
    finally:
        if not status:
            try:
                monitor.append(word)
            except ValueError as error:
                print(word)

    if option == 'false':
        try:
            bug_url = mp3_url
        except UnboundLocalError:
            bug_url = 'None'
        print("Hit: ", len(hit))
        return word, bug_url
    else:
        counter = counter + 1
        sys.stdout.write(f'\r\033[1;32m[+]\033[0m Downloading: {counter}')
        sys.stdout.flush()
    return 0

def stats():
    global wordlist, count
    print('\n[\033[1;34m*\033[0m] Summary\n\
    Sum:    \033[1;30m{}\033[0m\n\
    New:    \033[1;32m{}\033[0m\n\
    Failed: \033[1;31m{}\033[0m'.format(len(wordlist), count, 0))

def main():
    load()
    asyncio.run(watch())
    stats()

if __name__ == "__main__":
    main()
