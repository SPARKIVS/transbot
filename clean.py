#!/bin/python3

import re, time, sys, threading, robot, random

counter = 0
wordlist = []
words = set()

def replaceBrackets():
	line = line.replace(" ：", "：")
	if "知识点" in line:
		line = line.replace("知识点", "知识点 ")
		line = line.replace("知识点  ", "知识点 ")
		line = line.replace("\n", "") + "）" + "\n"
	line = line.replace("））", "）")
	if "知识点" in line:
		line = line.replace("）", ")")
		line = line.replace("（", "(")
	else:
		line = line.replace("）", "｝")
		line = line.replace("（", "｛")
	return

def obsolete():
	if "知识点" in line:
		g.write(line)
	elif "｛" in line:
		li = line.split("｛")
		g.write(li[0] + "\n")
		g.write("\n")
		g.write("- " + li[1])
	elif len(line) > 3:
		if "｝" in line:
			line = line.replace("｝", "")
		g.write("- " + line)
	else:
		g.write(line)
	return

def pickBoldword(line):
	global g, wordlist
	if '*' in line:
		if '｛' in line: 
			wList = line.split('｛')
			w1, w2 = wList[0], wList[1]
			if len(w1) > 0 and '*' in w1:
				wordlist.append(w1)
			elif len(w2) > 0 and '*' in w2:
				wordlist.append(w2)
		elif '(' in line:
			wList = line.split('(')
			w1, w2 = wList[0], wList[1]
			if len(w1) > 0 and '*' in w1:
				wordlist.append(w1)
			elif len(w2) > 0 and '*' in w2:
				wordlist.append(w2)
		else:
			wordlist.append(line)
	else:
		pass

def c3po(wl, translate=False):
	global words
	complete = []
	tasks = []

	translate=True

	def getTrans(w):
		global counter
		trans = ''
		for i in range(5):
			try:
				trans = robot.getChinese(w, stand_alone=False)
			except Exception as err:
				#print(i, err)
				time.sleep(random.randint(1, 10))
			else:
				if len(trans) == 0:
					time.sleep(random.randint(1, 10))
				else:
					break

		line = '|' + w + '|' + trans + '|' + '\n'
		complete.append(line)
		counter = counter + 1
		
		sys.stdout.write(f'\r\033[1;32m[+]\033[0m Counter: {counter}     {w}       ')
		sys.stdout.flush()

	for eachword in wl:
		for i in ['｛', '｝', '(', ')', '、', '；', ':', ' ，', '：', '*', '\n', 'n.', 'v.', 'adj.']:
			eachword = eachword.replace(i, '')
		eachword = eachword.strip()
		words.add(eachword)
	if translate:
		sargeking = []
		for word in words:
			task = threading.Thread(target=getTrans, args=(word, ))
			sargeking.append(task)
		for eachtask in sargeking:
			eachtask.start()
		for eachtask in sargeking:
			eachtask.join()
		complete.sort()
		g.write("| wordlist    | Translation |\n")
		g.write("| ----------- | ----------- |\n")
		for l in complete:
			g.write(l)
			#g.write('\n')
	else:
		with open('list.md', 'w') as file:
			for eachword in words:
				file.write(eachword + '\n')

f = open("./raw.md", "r")
g = open("./clean.md", "w+")
lines = f.readlines()
f.close()

# for line in lines:
# 	'''
# 	if "知识点" in line:
# 		pass
# 	else:
# 		pa = re.compile(r'[\u4e00-\u9fa5]')
# 		line = re.sub(pa, "", line)
# 	g.write(line)
# 	'''
# 	pickBoldword(line)
# c3po(wordlist)
c3po(lines)
g.close()
