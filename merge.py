from os import listdir
from pydub import AudioSegment
import sys

filelist = listdir("./audio")
filelist.sort()
filename = "./wordlist{}.mp3".format(len(filelist))

print("[*] %s mp3 audio to merge..." % len(filelist))

segment = AudioSegment.from_mp3("./audio/Silence01s.mp3")
silence = AudioSegment.from_mp3("./auodio/Silence02s.mp3")
x = AudioSegment.from_mp3("./audio/Silence01s.mp3")

counter = 0
for i in range(len(filelist)):
    x = x + AudioSegment.from_mp3("./audio/"+filelist[i]) + silence

    # pronun = AudioSegment.from_mp3("./audio/"+filelist[i])
    # x = x + pronun + segment + pronun + silence # read the pronunciation twice

    counter = counter + 1
    sys.stdout.write(f'\r\033[1;32m[+]\033[0m Counter: {counter}')
    sys.stdout.flush()

x.export(filename, format="mp3")
print("\nAudio Successfully Merged!")
